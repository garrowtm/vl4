import java.util.Random;

public class Main {
    public static void main(String[] args)
    {
        int size = 8;
        int[] array = new int[size];
        boolean is_sorted = true;
        Random rand = new Random();

        for (int i = 0; i < size; i++)
        {
            array[i] = Math.abs(rand.nextInt()) % 10 + 1;
        }

        for (int i = 0; i < size; i++)
        {
            System.out.print(array[i] + " ");
        }
        System.out.println();

        for (int i = 0; i < size - 1; i++)
        {
            if(array[i] >= array[i + 1])
            {
                is_sorted = false;
            }
        }

        if (is_sorted)
        {
            System.out.println("Массив является строго возрастающей последовательностью");
        } else
        {
            System.out.println("Массив НЕ является строго возрастающей последовательностью");
        }

        for (int i = 1; i < size; i += 2)
        {
            array[i] = 0;
        }

        for (int i = 0; i < size; i++)
        {
            System.out.print(array[i] + " ");
        }
    }
}